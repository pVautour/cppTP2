/*
* AVL.h
*
*  Created on: 5 avril, 2017
*  Author: Notes de cours inf3105-notes.pdf Bruno Malenfant
*/


#ifndef AVL_H_
#define AVL_H_

#include <assert.h>
#include <string>

using namespace std;

class ArbreAVL {

private:

  class Noeud {
  public:
    string contenu;
    Noeud * gauche, * droite;
    int equilibre, repetitions;
    Noeud(string contenu, Noeud * gauche, Noeud * droite){
      this->contenu = contenu;
      this->gauche = gauche;
      this->droite = droite;
      this->repetitions = 1;
    }

    void incr(){
      this->repetitions++;
    }
  };

  Noeud * racine = NULL;

  bool inserer(Noeud * & n, const string & element);
  int getRepetitions(Noeud * & n, const string & element);

public:
  float cote = 0.0;
  string titre;
  ArbreAVL();
  void inserer(const string & element);
  int getRepetitions(const string & element);
  void rotationGaucheDroite(Noeud * & racinesousarbre);
  void rotationDroiteGauche(Noeud * & racinesousarbre);

};

#endif /* AVL_H_ */
