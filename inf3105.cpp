
#include "DocumentXML.h"
#include "Histoire.h"
#include "AVL.h"

#include <math.h>
#include <iomanip>
#include <iostream>
#include <sstream>

using namespace std;

float calculIdf(string mot, vector<ArbreAVL*> *arbres);
float calculCote(const vector<string> mots, vector<ArbreAVL*> arbres, int indice);
void quickSort(vector<ArbreAVL*> *arbres, int debut, int nombreArbres);

vector< Histoire *> * lireDocuments( string a_nomFichier ) {
  vector<Histoire *> * histoires = new vector< Histoire * >();
  DocumentXML * listeFichiers = lireFichierXML( a_nomFichier );

  Element * courrant = listeFichiers->racine();
  vector< Contenu * >::const_iterator it = courrant->begin();

  // trouver <liste>
  while( ! ( * it )->estElement() ) ++ it;
  courrant = ( Element * )( * it );

  for( Contenu * contenu : * courrant ) {
    if( contenu->estElement() ) {
      Element * element = ( Element * )contenu;

      DocumentXML * doc = lireFichierXML( element->attribut( string( "fichier" ) ) );

      vector<Histoire *> * h = extraireHistoires( * doc );

      histoires->insert( histoires->end(), h->begin(), h->end() );
    }
  }

  return histoires;
}


int main() {

  vector<Histoire *> * histoires = lireDocuments( string( "listeDocument.xml" ) );
  vector<ArbreAVL *> arbres;
  int indice = -1;
  vector<string>::const_iterator it;

  for( Histoire * histoire : * histoires ) {
    ArbreAVL *arbre = new ArbreAVL();
    arbres.push_back(arbre);
    ++indice;
    // Parcours les mots
    for (it = histoire->begin(); it != histoire->end(); ++it){
      arbres[indice]->inserer(*it);
    }

    arbres[indice]->titre = ( * histoire ).titre();
  }

  //mots a chercher
  string mot;
  string input;
  bool quit = false;
  cout << "requete : ";
  while(!quit){
    getline(cin,input);
    if(input.size() > 0){
    vector<string> mots;
    istringstream iss(input);
    while (iss >> mot){
      mots.push_back(mot);
    }
    for (unsigned int i=0; i < arbres.size(); ++i){
      arbres[i]->cote = calculCote(mots, arbres, i);
    }
    quickSort(&arbres, 0, arbres.size());
    for (unsigned int i=0; i < arbres.size(); ++i){
      cout << arbres[i]->cote << " : " << arbres[i]->titre << endl;
      if(i == 4){
        i = arbres.size();
      }
    }
    cout << endl << "requete : ";
  } else {
    quit = true;
  }
  }
  return 0;
}

void quickSort(vector<ArbreAVL*> *arbres, int debut, int nombreArbres) {

  if(nombreArbres <= 1){
    return;
  }

  int pivot = nombreArbres / 2;
  swap((*arbres)[debut],(*arbres)[debut + pivot]);
  int k = 0;

  for(int i = 1; i < nombreArbres; i++){
    if((*arbres)[debut + i]->cote > (*arbres)[debut]->cote){
      ++k;
      swap((*arbres)[debut + k],(*arbres)[debut + i]);
    }
  }

  swap((*arbres)[debut],(*arbres)[debut + k]);
  quickSort(arbres, debut, k);
  quickSort(arbres, debut+k+1, nombreArbres-k-1);

}

float calculCote(const vector<string> mots, vector<ArbreAVL*> arbres, int indice){
  float cote = 0.0;
  vector<string>::const_iterator it;
  for (it = mots.begin(); it != mots.end(); ++it){
    cote += arbres[indice]->getRepetitions(*it) * calculIdf(*it, &arbres);
  }
  return cote;
}


float calculIdf( string mot,  vector<ArbreAVL*> *arbres){
  vector<ArbreAVL *>::const_iterator it;
  int occurences = 0;
  for (it = arbres->begin(); it != arbres->end(); ++it){
    if((*it)->getRepetitions(mot) > 0){
      ++occurences;
    }
  }
  return log2((float)arbres->size() / (float)occurences);
}
